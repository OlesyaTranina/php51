ymaps.ready(init);

function init () {
    var myMap = new ymaps.Map("map", {
        center:  [55.76, 37.64],
        zoom: 12,
        controls: []
    }, {
            //searchControlProvider: 'yandex#search'
        });
  
    var buttons = document.querySelectorAll(".btn");
    if (buttons.length>0){
         myMap.setCenter([buttons[0].dataset.latitude,  buttons[0].dataset.longitude]);
    };
    if (buttons.length==1){
         myMap.geoObjects
        .add(new ymaps.Placemark([buttons[0].dataset.latitude,  buttons[0].dataset.longitude], {
           iconContent: 'Тут'

        }, {
            preset: 'islands#dotIcon',
            iconColor: '#FA0606'
        })
        );
    };
    
    for (i = 0; i < buttons.length; i++) {
      buttons[i].addEventListener("click",function(event){
        var coord =  [this.dataset.latitude,this.dataset.longitude];
        myMap.geoObjects
        .add(new ymaps.Placemark(coord, {
           iconContent: 'Тут'

        }, {
            preset: 'islands#dotIcon',
            iconColor: '#FA0606'
        })
        );
        myMap.setCenter(coord);
         
    });
  }
}

