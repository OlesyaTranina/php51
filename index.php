<?php
error_reporting(E_ALL);
ini_set("display_errors", 0); 
require __DIR__.'/vendor/autoload.php';

if (isset($_GET["address"])) {
$api = new \Yandex\Geo\Api();
$api->setQuery($_GET["address"]);

$api->setLang(\Yandex\Geo\Api::LANG_RU) 
    ->load();

$response = $api->getResponse();
$response->getFoundCount(); 
$response->getQuery(); 
$response->getLatitude(); 
$response->getLongitude();
// Список найденных точек
$collection = $response->getList();
} else {
    $collection = array();
};


?>
<!DOCTYPE html>
<html>
<head>
    <title>Определение координат</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!-- Если вы используете API локально, то в URL ресурса необходимо указывать протокол в стандартном виде (http://...)-->
    <script src="http://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <script src="mapbasics.js" type="text/javascript"></script>
    <style>
        html, body, #map {
            width: 100%; height: 100%; padding: 0; margin: 0;
        }
        table { 
            border-spacing: 0;
            border-collapse: collapse;
        }

        table td, table th {
            border: 1px solid black;
            padding: 5px; 
        }

        table th {
            background: lightgray;
        }

    </style>
</head>
<body>
    <h1>Найти координаты по адресу</h1>
    <form metod="GET">
        <label for="address">Введите адрес</label>
        <input type="text" name="address" size=100 value=<?php if (isset($_GET["address"])){
                                                            echo '"'. htmlspecialchars($_GET["address"]) .'"';
                                                                } ?>>  
        <input type="submit" value="Найти координаты" />
    </form>
    <table>
    <tr>
        <th>адрес</th>
        <th>широта</th>
        <th>долгота</th>
        <th>показать на карте</th>
    </tr>
    <?php foreach ($collection as $item) { ?>
    <tr>
    <td> <?php  echo $item->getAddress(); ?></td>
        <td> <?php echo $item->getLatitude(); ?></td>
        <td> <?php echo $item->getLongitude(); ?></td>
        <td> <button class="btn" data-Latitude=<?=$item->getLatitude()?> data-Longitude=<?=$item->getLongitude()?>>показать</button></td>
    </tr>
   <?php } ?>
    </table>
    <div id="map"></div>
</body>
</html>